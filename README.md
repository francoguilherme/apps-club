Mock API usada:
https://private-291f64-appsclub1.apiary-mock.com/

Os aplicativos são organizados da seguinte forma:
https://private-291f64-appsclub1.apiary-mock.com/"código do país"/"pacote(apps, kids ou games)"/

Por exemplo, para acessar os games disponíveis no Brasil:
https://private-291f64-appsclub1.apiary-mock.com/br/games

Para fins de teste, só utilizei três países: Brasil, Estados Unidos, e Inglaterra, com os códigos br, us, uk respectivamente.

Brasil só contém aplicativos com descrição em português, Inglaterra só com descrição em inglês e Estados Unidos contém todos os aplicativos.

Observação: No código, o código de país para Inglaterra é na verdade "gb", de Great Britain, pois este é o padrão ISO.

Os três diferentes pacotes do Apps Club (Apps, Kids e Games) estão definidos como FLAVORS no projeto. Cada um resulta em um aplicativo com nome e ícone diferente.